#!/bin/sh
set -e

export PYTHONPATH=$PYTHONPATH:/project/src/

if [ "$1" = 'app' ]; then
    echo 'Run wallet app'
    exec python3 /project/src/app.py
elif [ "$1" = 'tests' ]; then
    echo 'Run tests'
    exec python3 -m unittest discover -s "tests"
else
    exec "$@"
fi