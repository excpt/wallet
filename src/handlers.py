from peewee import fn, IntegrityError
from sanic.response import json

import settings
from models import CurrencyBalance, Transaction
import models

BALANCE_RESULT_TEMPLATE = dict(zip(settings.CURRENCIES, [0] * len(settings.CURRENCIES)))

BadRequestResponse = json({'error': 'Bad request'}, status=400)


class ParseError(Exception):
    pass


def parse_change_balance_data(request):
    try:
        user_id = abs(int(request.json['user_id']))
    except (KeyError, ValueError, TypeError):
        raise ParseError

    currency = str(request.json.get('currency', '')).lower()
    transaction_id = request.json.get('transaction_id')

    if not transaction_id or type(transaction_id) != str:
        raise ParseError

    if currency not in settings.CURRENCIES:
        raise ParseError

    try:
        value = abs(float(request.json['value']))
    except (ValueError, KeyError, TypeError):
        raise ParseError

    return user_id, currency, value, transaction_id


async def get_balance_handler(request):
    try:
        user_id = int(request.raw_args['user_id'])
    except (KeyError, ValueError, TypeError):
        return BadRequestResponse

    currency = request.raw_args.get('currency', '').lower()
    query = CurrencyBalance.filter(user_id=user_id)

    if currency and currency in settings.CURRENCIES:
        query = query.filter(currency=currency)
        result = {currency: 0}
    else:
        result = BALANCE_RESULT_TEMPLATE.copy()

    balances = await models.objects.execute(query)
    [result.__setitem__(b.currency, b.value) for b in balances]

    return json({'result': result}, ensure_ascii=False)


async def update_or_create_balance(user_id, currency, value, transaction_id):
    try:
        async with models.objects.atomic():
            balance, _ = await models.objects.get_or_create(
                CurrencyBalance,
                user_id=user_id,
                currency=currency,
            )
            query = CurrencyBalance.update(value=CurrencyBalance.value + value)\
                .where(CurrencyBalance.user_id == user_id,
                       CurrencyBalance.currency == currency)

            if value < 0:
                query = query.where(CurrencyBalance.value >= abs(value))

            updated = await models.objects.execute(query)

            if value < 0 and not updated:
                return json({'error': 'Not enough balance value'}, status=409)

            await models.objects.create(Transaction, id=transaction_id, balance=balance)
    except IntegrityError:
        return json({'error': 'Transaction failed'}, status=409)

    return json({'result': 'success'}, ensure_ascii=False)


async def make_credit_handler(request):
    try:
        user_id, currency, value, transaction_id = parse_change_balance_data(request)
    except ParseError:
        return BadRequestResponse

    return await update_or_create_balance(user_id, currency, -value, transaction_id)


async def make_debit_handler(request):
    try:
        user_id, currency, value, transaction_id = parse_change_balance_data(request)
    except ParseError:
        return BadRequestResponse

    return await update_or_create_balance(user_id, currency, value, transaction_id)
