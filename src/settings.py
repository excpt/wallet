USD = 'usd'
EUR = 'eur'
GBP = 'gbp'

CURRENCIES = [USD, EUR, GBP]

POSTGRES_HOST = 'db'
POSTGRES_DB_NAME = 'wallet_db'
POSTGRES_DB_PASS = 'password'
POSTGRES_DB_USER = 'user'
