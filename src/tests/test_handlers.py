import asyncio
from json import loads
from unittest import TestCase
from unittest.mock import patch

from handlers import get_balance_handler, make_debit_handler, update_or_create_balance, \
    ParseError, BadRequestResponse, make_credit_handler, parse_change_balance_data
from models import CurrencyBalance, Transaction, DBConnection, connect_db_manager
from settings import USD, EUR, GBP


def run_sync(coro, loop=None):
    if not loop:
        loop = asyncio.get_event_loop()
    return loop.run_until_complete(coro)


class FakeRequest():
    def __init__(self, **kwargs):
        [setattr(self, k, v) for k, v in kwargs.items()]


def decode_response(response):
    return loads(response.body.decode())


def fake_coro(result=None):
    return asyncio.coroutine(lambda *x, **y: result)()


class WalletHandlersTest(TestCase):
    _tables = (CurrencyBalance, Transaction)

    @property
    def loop(self):
        if not hasattr(self, '_loop'):
            self._loop = asyncio.get_event_loop()
        return self._loop

    @classmethod
    def setUpClass(cls):
        db = DBConnection()
        db.drop_tables(cls._tables, safe=True, cascade=True)
        db.create_tables(cls._tables, safe=True,)

    def setUp(self):
        connect_db_manager(self.loop)

    def tearDown(self):
        Transaction.delete().execute()
        CurrencyBalance.delete().execute()

    def test_get_balance_one_balance(self):
        CurrencyBalance.create(user_id=1, currency=USD, value=100.1)
        request = FakeRequest(raw_args=dict(user_id=1))
        response = run_sync(get_balance_handler(request), self.loop)
        self.assertEqual(decode_response(response),
                         {'result': {USD: 100.1, EUR: 0.0, GBP: 0}})

    def test_get_balance_currency(self):
        CurrencyBalance.create(user_id=1, currency=USD, value=100.1)
        request = FakeRequest(raw_args=dict(user_id=1, currency=USD))
        response = run_sync(get_balance_handler(request), self.loop)
        self.assertEqual(decode_response(response), {'result': {USD: 100.1}})

    def test_get_balance_all_balances(self):
        CurrencyBalance.create(user_id=1, currency=USD, value=100.1)
        CurrencyBalance.create(user_id=1, currency=EUR, value=200.2)
        CurrencyBalance.create(user_id=1, currency=GBP, value=300.3)
        request = FakeRequest(raw_args=dict(user_id=1))
        response = run_sync(get_balance_handler(request), self.loop)
        self.assertEqual(decode_response(response),
                         {'result': {USD: 100.1, EUR: 200.2, GBP: 300.3}})

    def test_get_balance_user_doesnt_exist(self):
        request = FakeRequest(raw_args=dict(user_id=9999))
        response = run_sync(get_balance_handler(request), self.loop)
        self.assertEqual(decode_response(response),
                         {'result': {USD: 0, EUR: 0, GBP: 0}})

    def test_create_balance(self):
        eur_balance = CurrencyBalance.create(user_id=1, currency=EUR, value=2)
        neigh_usd_balance = CurrencyBalance.create(user_id=2, currency=USD, value=3)
        neigh_eur_balance = CurrencyBalance.create(user_id=2, currency=EUR, value=4)
        response = run_sync(
            update_or_create_balance(user_id=1, currency=USD, value=100.1,
                                                     transaction_id='1234567890'),
            self.loop,
        )
        usd_balance = CurrencyBalance.get(user_id=1, currency=USD)
        eur_balance.reload()
        neigh_usd_balance.reload()
        neigh_eur_balance.reload()
        self.assertEqual(usd_balance.value, 100.1)
        self.assertEqual(eur_balance.value, 2)
        self.assertEqual(neigh_usd_balance.value, 3)
        self.assertEqual(neigh_eur_balance.value, 4)
        self.assertEqual(decode_response(response), {'result': 'success'})

    def test_update_balance_increment(self):
        eur_balance = CurrencyBalance.create(user_id=1, currency=EUR, value=1)
        usd_balance = CurrencyBalance.create(user_id=1, currency=USD, value=100)
        neigh_eur_balance = CurrencyBalance.create(user_id=2, currency=EUR, value=2)
        response = run_sync(
            update_or_create_balance(user_id=1, currency=USD, value=100.1,
                                     transaction_id='1234567890'),
            self.loop,
        )
        usd_balance.reload()
        eur_balance.reload()
        neigh_eur_balance.reload()
        self.assertEqual(usd_balance.value, 200.1)
        self.assertEqual(eur_balance.value, 1)
        self.assertEqual(neigh_eur_balance.value, 2)
        self.assertEqual(decode_response(response), {'result': 'success'})

    def test_update_balance_decrement_success(self):
        eur_balance = CurrencyBalance.create(user_id=1, currency=EUR, value=1)
        usd_balance = CurrencyBalance.create(user_id=1, currency=USD, value=100)
        neigh_eur_balance = CurrencyBalance.create(user_id=2, currency=EUR, value=2)
        response = run_sync(
            update_or_create_balance(user_id=1, currency=USD, value=-9.9,
                                     transaction_id='1234567890'),
            self.loop,
        )
        usd_balance.reload()
        eur_balance.reload()
        neigh_eur_balance.reload()
        self.assertEqual(usd_balance.value, 90.1)
        self.assertEqual(eur_balance.value, 1)
        self.assertEqual(neigh_eur_balance.value, 2)
        self.assertEqual(decode_response(response), {'result': 'success'})

    def test_update_balance_decrement_failed(self):
        eur_balance = CurrencyBalance.create(user_id=1, currency=EUR, value=1)
        usd_balance = CurrencyBalance.create(user_id=1, currency=USD, value=2)
        neigh_eur_balance = CurrencyBalance.create(user_id=2, currency=EUR, value=2)
        response = run_sync(
            update_or_create_balance(user_id=1, currency=USD, value=-9.9,
                                     transaction_id='1234567890'),
            self.loop,
        )
        usd_balance.reload()
        eur_balance.reload()
        neigh_eur_balance.reload()
        self.assertEqual(usd_balance.value, 2)
        self.assertEqual(eur_balance.value, 1)
        self.assertEqual(neigh_eur_balance.value, 2)
        self.assertEqual(decode_response(response), {'error': 'Not enough balance value'})

    def test_update_balance_transaction_failed(self):
        balance = CurrencyBalance.create(user_id=1, currency=USD, value=100)
        neigh_balance = CurrencyBalance.create(user_id=2, currency=EUR, value=1)

        Transaction.create(balance=balance, id='1234567890')
        response = run_sync(
            update_or_create_balance(user_id=1, currency=USD, value=100.1,
                                     transaction_id='1234567890'),
            self.loop,
        )
        balance.reload()
        neigh_balance.reload()
        self.assertEqual(balance.value, 100)
        self.assertEqual(neigh_balance.value, 1)
        self.assertEqual(decode_response(response), {'error': 'Transaction failed'})

    @patch('handlers.parse_change_balance_data', side_effect=ParseError)
    @patch('handlers.update_or_create_balance', return_value=fake_coro())
    def test_make_credit_handler_parse_error(self, update_mock, parse_mock):
        request = FakeRequest()
        response = run_sync(make_credit_handler(request), self.loop)
        self.assertEqual(update_mock.called, False)
        self.assertEqual(response, BadRequestResponse)

    @patch('handlers.parse_change_balance_data', return_value=(1, 2, 3, 4))
    @patch('handlers.update_or_create_balance', return_value=fake_coro(12345))
    def test_make_credit_handler_success(self, update_mock, parse_mock):
        request = FakeRequest()
        response = run_sync(make_credit_handler(request), self.loop)
        parse_mock.assert_called_with(request)
        update_mock.assert_called_with(1, 2, -3, 4)
        self.assertEqual(response, 12345)

    @patch('handlers.parse_change_balance_data', side_effect=ParseError)
    @patch('handlers.update_or_create_balance', return_value=fake_coro())
    def test_make_debit_handler_parse_error(self, update_mock, parse_mock):
        request = FakeRequest()
        response = run_sync(make_debit_handler(request), self.loop)
        self.assertEqual(update_mock.called, False)
        self.assertEqual(response, BadRequestResponse)

    @patch('handlers.parse_change_balance_data', return_value=(1, 2, 3, 4))
    @patch('handlers.update_or_create_balance', return_value=fake_coro(12345))
    def test_make_debit_handler_success(self, update_mock, parse_mock):
        request = FakeRequest()
        response = run_sync(make_debit_handler(request), self.loop)
        parse_mock.assert_called_with(request)
        update_mock.assert_called_with(1, 2, 3, 4)
        self.assertEqual(response, 12345)

    def test_parse_change_balance_data_success(self):
        request = FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                            value=123.99, user_id=9876))
        user_id, currency, value, tx_id = parse_change_balance_data(request)
        self.assertEqual(user_id, 9876)
        self.assertEqual(currency, EUR)
        self.assertEqual(value, 123.99)
        self.assertEqual(tx_id, 'TX12345-0099')

    def test_parse_change_balance_data_wrong_value(self):
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                      value='1asd\01\02', user_id=9876))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                      value='', user_id=9876))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                      user_id=9876))
            )

    def test_parse_change_balance_data_wrong_currency(self):
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='', transaction_id='TX12345-0099',
                                      value=1, user_id=9876))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur1', transaction_id='TX12345-0099',
                                      value=1, user_id=9876))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(transaction_id='TX12345-0099',
                                      value=1, user_id=9876))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency=1, transaction_id='TX12345-0099',
                                      value=1, user_id=9876))
            )

    def test_parse_change_balance_data_wrong_user_id(self):
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                      value=1, user_id=''))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                      value=1, user_id='1asd\01\02'))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='TX12345-0099',
                                      value=1, ))
            )

    def test_parse_change_balance_data_wrong_transaction_id(self):
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id='',
                                      value=1, user_id=123))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', value=1, user_id=123))
            )
        with self.assertRaises(ParseError):
            parse_change_balance_data(
                FakeRequest(json=dict(currency='eur', transaction_id=23,
                                      value=1, user_id=123))
            )
