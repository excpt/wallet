import peewee
import peewee_async

import settings


class DBConnection(object):
    instance = None

    def __init__(self):
        if not self.instance:
            DBConnection.instance = peewee_async.PooledPostgresqlDatabase(
                settings.POSTGRES_DB_NAME,
                user=settings.POSTGRES_DB_USER,
                password=settings.POSTGRES_DB_PASS,
                host=settings.POSTGRES_HOST,
                port=5432,
            )

    def __getattr__(self, item):
        return getattr(self.instance, item)


def connect_db_manager(loop):
    global objects
    objects = peewee_async.Manager(DBConnection().instance, loop=loop)


class BaseModel(peewee.Model):
    class Meta:
        database = DBConnection().instance

    def reload(self):
        newer_self = self.get(self._meta.primary_key == self._get_pk_value())
        for field_name in self._meta.fields.keys():
            val = getattr(newer_self, field_name)
            setattr(self, field_name, val)
        self._dirty.clear()


class CurrencyBalance(BaseModel):
    id = peewee.PrimaryKeyField()
    currency = peewee.CharField(max_length=5)
    value = peewee.FloatField(default=0)
    user_id = peewee.IntegerField()


class Transaction(BaseModel):
    id = peewee.CharField(unique=True, max_length=255, primary_key=True)
    balance = peewee.ForeignKeyField(CurrencyBalance)

    class Meta:
        indexes = (
            (("id", "balance"), True),
        )
