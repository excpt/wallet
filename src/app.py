from sanic import Sanic

from handlers import get_balance_handler, make_credit_handler, make_debit_handler
import models


async def before_start(app, loop):
    models.connect_db_manager(loop)


if __name__ == "__main__":
    db = models.DBConnection().instance
    db.create_tables((models.CurrencyBalance, models.Transaction), safe=True, )

    app = Sanic()
    app.add_route(get_balance_handler, '/get_balance', methods=frozenset({'GET'}))
    app.add_route(make_credit_handler, '/credit', methods=frozenset({'POST'}))
    app.add_route(make_debit_handler, '/debit', methods=frozenset({'POST'}))
    app.listeners['before_server_start'].append(before_start)
    app.run(host="0.0.0.0", port=80)
