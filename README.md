# wallet

### endpoints
 - GET /get_balance
 - POST /credit
 - POST /debit

### running
 - `docker-compose up -d`
 - app listen 8080 port

### tests running
`docker-compose run app tests`

### example
```sh
curl -XPOST http://127.0.0.1:8080/debit -H "content-type: application/json" -d '{"user_id": 1, "value": 200, "currency": "usd", "transaction_id": "12345"}'
curl -XPOST http://127.0.0.1:8080/credit -H "content-type: application/json" -d '{"user_id": 1, "value": 25, "currency": "usd", "transaction_id": "12346"}'
curl http://127.0.0.1:8080/get_balance?user_id=1&currency=usd
```