FROM python:3.6-slim

MAINTAINER Ilya Grinzovskiy <ilya.grinzovskiy@gmail.com>

RUN apt-get update -qy && apt-get upgrade -qy
RUN apt-get install -qy libcurl4-openssl-dev \
                        gcc \
                        libffi-dev \
                        software-properties-common \
                        sudo \
                        git \
                        lib32z1-dev \
                        libpq-dev \
                        gettext

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV TERM xterm
RUN bash -c 'echo "export LC_ALL=C.UTF-8" >> ~/.bashrc'

ADD /requirements.txt /project/
RUN pip3 install -r /project/requirements.txt

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY /src /project/src
ADD /entrypoint.sh /project/
ENTRYPOINT ["/project/entrypoint.sh"]
RUN chmod 755 /project/entrypoint.sh

WORKDIR /project/src

EXPOSE 80